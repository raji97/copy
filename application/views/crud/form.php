<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		table {
		    font-family: arial, sans-serif;
		    border-collapse: collapse;
		    width: 100%;
		}

		td, th {
		    border: 1px solid #dddddd;
		    text-align: center;
		    padding: 8px;
		}

		tr:nth-child(even) {
		    background-color: #dddddd;
		}
	</style>
	<title></title>
</head>
<body>
	<form action="<?= site_url('crud/save'); ?>" method="POST">
		<table style="width:50%">
		  <tr>
		    <td>Nama Anda </td>
		    <td>
		    	<input type="text" name="nama" value="<?= @$nama;?>">
		    </td>
		  </tr>

		  <tr>
		    <td>Tanggal Lahir </td>
		    <td>
		    	<input type="date" name="tanggal" value="<?= @$tanggal;?>">
		    </td>
		  </tr>
		  <tr>
		    <td>Alamat Anda </td>
		    <td>
		    	<textarea name="alamat"><?=@$alamat;?></textarea>
		    </td>
		    <tr>
			    <td><input type="submit" name="submit" value="Simpan"></td>
			</tr>
		  </tr>
		</table>
	</form>
</body>
</html>