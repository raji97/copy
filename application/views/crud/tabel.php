<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		table {
		    font-family: arial, sans-serif;
		    border-collapse: collapse;
		    width: 100%;
		}

		td, th {
		    border: 1px solid #dddddd;
		    text-align: center;
		    padding: 8px;
		}

		tr:nth-child(even) {
		    background-color: #dddddd;
		}
	</style>
	<title></title>
</head>
<body>
	<table style="width:50%">
	  <tr>
	    <th>No</th>
	    <th>Nama</th>
	    <th>Tanggal Lahir</th> 
	    <th>Alamat</th>
	    <th>Action</th>
	  </tr>
	  <?php foreach($data as $data){ ?>
	  <tr>
	    <td><?= $data['id']; ?></td>
	    <td><?= $data['nama']; ?></td>
	    <td><?= $data['tanggal']; ?></td> 
	    <td><?= $data['alamat']; ?></td>
	    <td>
	    	<a href="<?= site_url('crud/edit/'.$data['id']); ?>">Edit</a>
	    	<a href="<?= site_url('crud/delete/'. $data['id']);?>">Delete</a>
	    </td>
	  </tr>
	  <?php } ?>
	</table>
	<a href="<?= site_url('crud/add');?>">Tambah</a>
</body>
</html>