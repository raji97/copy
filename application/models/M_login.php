<?
/**
* 
*/
class M_login extends CI_Model{
	
	public function login($user, $pass){

		$this->db->select('username, password');
		$this->db->from('tbl_user');
		$this->db->where('username', $user);
		$this->db->where('password', $pass);
		$this->db->limit(1);

		$data = $this->db->get();

		if($data->num_rows() == 1 )
		{
			return $data->result();
		}else{
			return false;
		}

	}
}
?>