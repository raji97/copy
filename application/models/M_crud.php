<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_crud extends CI_Model {

	public function getCrud(){
		$data = $this->db->query('SELECT * FROM tbl_anggota ');
		return $data->result_array();
	}

	public function insert($tableName,$data){
		$tampung = $this->db->insert($tableName,$data);
		return $tampung;
	}

	public function update($tableName,$data,$where){
		$tampung = $this->db->update($tableName,$data,$where);
		return $tampung;
	}

	public function hapus($tableName,$where){
		$tampung = $this->db->delete($tableName,$where);
		return $tampung;
	}

	public function edit($where){
		$data = $this->db->query('SELECT * FROM tbl_anggota '.$where);
		return $data->row_array();
	}
}
