<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_crud');
	}


	public function index()
	{
		$data = $this->M_crud->getCrud();
		$this->load->view('crud/tabel', array('data' => $data));
		// print_r($data);
	}

	public function add(){
		$this->load->view('crud/form');
	}

	public function save(){
		// print_r($_POST);
		$nama = $this->input->post('nama');
		$tanggal = $this->input->post('tanggal');
		$alamat = $this->input->post('alamat');

		$data = array(
			'nama' => $nama,
			'tanggal' => $tanggal,
			'alamat' => $alamat
		);

		// $this->db->insert('tbl_anggota', $data);
		$data = $this->M_crud->insert('tbl_anggota', $data);

		if( $data>=1 ){
			echo "Berhasil";
			redirect('crud');
		}else{
			echo "Gagal";
		}
	}

	public function edit($id){
		$where = array('id' => $id);
		$data = $this->M_crud->edit('tbl_anggota', $where);
		$this->load->view('crud/form', $data);

	}

	public function delete($id){
		$where = array('id' => $id);
		$data = $this->M_crud->hapus('tbl_anggota', $where);

		if($data>=1){
			echo "Berhasil menghapus data";
		}

		redirect('crud');
	}
}
